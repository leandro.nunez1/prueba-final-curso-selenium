package dataproviders;

import entities.User;
import org.testng.annotations.DataProvider;
import utils.Utils;

import java.util.Iterator;

public class UsersData {

    @DataProvider(name = "users")
    public static Object[][] getUserData() {
        return new Object[][]{
                {"Test1", "Test1", Utils.generateRandomEmail(), "Test1234", "Test1234"},
                {"Test2", "Test2", Utils.generateRandomEmail(), "Test1234", "Test1234"}
        };
    }

}
