package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPage extends HeaderPage {

    public AccountPage(WebDriver driver) {

        super(driver);

        waitForElementToAppear(sideMenuTitle);

    }

    @FindBy(xpath = "//*[@class='block-title']/strong/span[text()='My Account']")
    private WebElement sideMenuTitle;

    private WebElement getSideMenuSubtabLnk(String subtab) {
            return driver.findElement(By.xpath("//*[@class='block-content']/ul/li/a/" +
                    "strong[contains(text(),'" + subtab + "')]"));
    }

    public WishListPage goToWishlist() {

        getSideMenuSubtabLnk("My Wishlist");

        return new WishListPage(driver);

    }

}
