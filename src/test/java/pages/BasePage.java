package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {

    private static final int TIMEOUT = 5;
    private static final int POLLING = 100;

    protected WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver driver) {

        this.driver = driver;

        wait = new WebDriverWait(driver, Duration.ofSeconds(TIMEOUT), Duration.ofSeconds(POLLING));

        PageFactory.initElements(driver,this);

    }

    @FindBy(xpath = "//*[contains(@class,\"page-title\")]/h1")
    private WebElement pageTitle;

    @FindBy(xpath = "//li[@class='success-msg']/ul/li/span")
    private WebElement successMsgAlert;

    public WebElement getPageTitle() {
        return pageTitle;
    }

    public WebElement getSuccessMsgAlert() {
        return successMsgAlert;
    }

    public WebElement getOptionWithText(String text) {
        return driver.findElement(By.xpath("//option[text()='" + text + "']"));
    }

    public WebElement getProductTitleByName(String productName) {

        return driver.findElement(By.xpath("//*[@title=\"" + productName
                + "\"]/parent::*[@class='product-name']"));

    }

    public WebElement getPriceByProductName(String productName) {

        return driver.findElement(By.xpath("//*[@title=\"" + productName
                + "\"]/parent::*[@class='product-name']/following-sibling::*[@class='price-box']/" +
                "*[@class='regular-price']/*[@class='price']"));

    }

    protected void waitForElementToAppear(WebElement elem) {

        try {

            wait.until(ExpectedConditions.visibilityOf(elem));

        } catch(Exception e) {

            throw new WebDriverException("Element " + elem + " not displayed after " + TIMEOUT + " seconds");

        }

    }

    protected void waitForElementToDisappear(WebElement elem) {

        try {

            wait.until(ExpectedConditions.invisibilityOf(elem));

        } catch(Exception e) {

            throw new WebDriverException("Element " + elem + " still displayed after " + TIMEOUT + " seconds");

        }

    }

    protected void waitForTextToDisappear(WebElement elem, String text) {

        try {

            wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(elem, text.toUpperCase())));

        } catch(Exception e) {

            throw new WebDriverException("Text \"" + text + "\" still displayed in element " + elem + " after " + TIMEOUT + " seconds");

        }

    }

    protected void waitForTextToAppear(WebElement elem, String text) {

        try {

                wait.until(ExpectedConditions.textToBePresentInElement(elem, text.toUpperCase()));

        } catch(Exception e) {

            throw new WebDriverException("Text \"" + text + "\" not still displayed in element " + elem + " after " + TIMEOUT + " seconds");

        }

    }

    public void sendText(WebElement e, String text) {

        e.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));

        e.sendKeys(text);

    }

}
