package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends HeaderPage{

    public CartPage(WebDriver driver) {

        super(driver);

        waitForTextToAppear(getPageTitle(), "Shopping Cart");

    }

    @FindBy(xpath = "//h2[@class=\"product-name\"]/a")
    private WebElement productNameLnk;


    @FindBy(xpath = "//span[@class=\"cart-price\"]/span[@class=\"price\"]")
    private WebElement productPrice;

    public WebElement getProductNameLnk() {
        return productNameLnk;
    }

    public WebElement getProductPrice() {
        return productPrice;
    }

}
