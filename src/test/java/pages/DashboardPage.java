package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends AccountPage {

    public DashboardPage(WebDriver driver) {

        super(driver);

        waitForTextToAppear(getPageTitle(), "MY DASHBOARD");

    }

    @FindBy(xpath = "//p[@class=\"hello\"]/strong")
    private WebElement helloMsgStr;

    public WebElement getHelloMsgStr() { return this.helloMsgStr; }

    @FindBy(xpath = "//*[contains(text(),\"Contact Information\")]/parent::div[@class=\"box-title\"]" +
            "/following-sibling::div[@class=\"box-content\"]/p")
    private WebElement contactInfoTxt;

    public WebElement getContactInfoTxt() {
        return this.contactInfoTxt;
    }

}
