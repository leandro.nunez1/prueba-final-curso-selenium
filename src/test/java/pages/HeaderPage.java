package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderPage extends BasePage {

    public HeaderPage(WebDriver driver) {

        super(driver);

        waitForElementToAppear(this.accountLnkDDL);

    }

    @FindBy(css = ".skip-account")
    private WebElement accountLnkDDL;

    @FindBy(xpath = "//a[@title=\"Register\"]")
    private WebElement registerLnk;

    @FindBy(xpath = "//a[@title=\"Log In\"]")
    private WebElement loginLnk;

    @FindBy (id = "search")
    private WebElement searchInput;

    @Step("Goes to register account page")
    public RegisterAccountPage goToRegisterAccount() {

        this.accountLnkDDL.click();

        this.registerLnk.click();

        return new RegisterAccountPage(driver);

    }

    @Step("Goes to login page")
    public LoginPage goToLogin() {

        this.accountLnkDDL.click();

        this.loginLnk.click();

        return new LoginPage(driver);

    }

    @Step("Searches product with name = \"{0}\" via search bar")
    public SearchProductPage searchItem(String productName) {

        sendText(this.searchInput, productName);

        this.searchInput.sendKeys(Keys.ENTER);

        return new SearchProductPage(driver);

    }
}
