package pages;

import entities.User;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends HeaderPage {

    public LoginPage(WebDriver driver) {

        super(driver);

        waitForElementToAppear(this.loginBtn);

    }

    @FindBy(xpath = "//button[@title=\"Login\"]")
    private WebElement loginBtn;

    @FindBy (id = "email")
    private WebElement emailInput;

    @FindBy (id = "pass")
    private WebElement passwordInput;

    private void fillForm(User user) {

        sendText(emailInput, user.getEmail());

        sendText(passwordInput, user.getPassword());

    }

    @Step("Logs in")
    public DashboardPage login(User user) {

        fillForm(user);

        loginBtn.click();

        return new DashboardPage(driver);

    }


}
