package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends HeaderPage {

    public ProductPage(WebDriver driver) {

        super(driver);

        waitForElementToAppear(addToCartBtn);

    }

    @FindBy(className = "btn-cart")
    private WebElement addToCartBtn;

    @FindBy(xpath = "//*[@class='product-name']/span")
    private WebElement productTitle;

    @FindBy(className = "price")
    private WebElement productPrice;

    @FindBy(id = "attribute92")
    private WebElement colorSelectDDL;

    @FindBy(id = "attribute180")
    private WebElement sizeSelectDDL;

    @FindBy(id = "qty")
    private WebElement quantityInput;

    public WebElement getProductTitle() {
        return productTitle;
    }

    public WebElement getProductPrice() {
        return productPrice;
    }

    private void selectColor(String color) {

        colorSelectDDL.click();

        getOptionWithText(color).click();

    }

    private void selectSize(String size) {

        sizeSelectDDL.click();

        getOptionWithText(size).click();

    }

    @Step("Adds product to cart")
    public CartPage addToCart(String color, String size, String quantity) {

        selectColor(color);

        selectSize(size);

        sendText(quantityInput, quantity);

        addToCartBtn.click();

        return new CartPage(driver);

    }

}
