package pages;

import entities.User;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterAccountPage extends HeaderPage {

    public RegisterAccountPage(WebDriver driver) {

        super(driver);

        waitForElementToAppear(this.registerBtn);

    }

    @FindBy (id = "firstname")
    private WebElement firstnameInput;

    @FindBy (id = "lastname")
    private WebElement lastNameInput;

    @FindBy (id = "email_address")
    private WebElement emailInput;

    @FindBy (id = "password")
    private WebElement passwordInput;

    @FindBy (id = "confirmation")
    private WebElement confirmPasswordInput;

    @FindBy (xpath = "//button[@title=\"Register\"]")
    private WebElement registerBtn;


    private void fillForm(User user) {

        sendText(firstnameInput, user.getFirstName());

        sendText(lastNameInput, user.getLastName());

        sendText(emailInput, user.getEmail());

        sendText(passwordInput, user.getPassword());

        sendText(confirmPasswordInput, user.getConfirmPassword());

    }

    @Step("Registers a new user")
    public DashboardPage registerAccount(User user) {

        fillForm(user);

        registerBtn.click();

        return new DashboardPage(driver);

    }



}
