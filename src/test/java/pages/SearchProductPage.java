package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchProductPage extends HeaderPage {

    public SearchProductPage(WebDriver driver) {

        super(driver);

        waitForTextToAppear(getPageTitle(), "Search results for");

    }

    private WebElement getWishlistLnkWithProductName(String productName) {
        return driver.findElement(By.xpath("//*[@title=\"" + productName
                + "\"]/parent::*[@class='product-name']/following-sibling::*[@class='actions']/" +
                "descendant::a[@class='link-wishlist']"));
    }

    @Step("Adds \"{0}\" to wishlist")
    public WishListPage addToWishList(String productName) {

        getWishlistLnkWithProductName(productName).click();

        return new WishListPage(driver);

    }

}
