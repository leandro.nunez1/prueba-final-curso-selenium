package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WishListPage extends AccountPage {

    public WishListPage(WebDriver driver) {

        super(driver);

        waitForTextToAppear(getPageTitle(), "My Wishlist");

    }

    @Override
    public WebElement getPriceByProductName(String productName) {

        return driver.findElement(By.xpath("//*[@title=\"" + productName +
                "\"]/ancestor::*[contains(@class,'customer-wishlist-item-info')]/" +
                "following-sibling::*[contains(@class,'customer-wishlist-item-price')]/" +
                "descendant::span[@class='price']"));

    }


}
