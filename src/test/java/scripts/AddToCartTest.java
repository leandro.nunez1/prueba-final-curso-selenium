package scripts;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HeaderPage;
import pages.ProductPage;
import pages.SearchProductPage;
import utils.Utils;

import static org.testng.Assert.assertTrue;

public class AddToCartTest extends BaseTest{

    private String product = "Stretch Cotton Blazer";
    private String price = "$490.00";
    private String color = "Blue";
    private String size = "XS";
    private String quantity = "1";

    @Test
    @Description("Should be able to search for an existing product via search bar then add it to cart")
    public void addToCart() throws Exception {

        Utils.log("Opens main url");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        SearchProductPage searchProductPage = headerPage.searchItem(product);

        Utils.log("Checks if searched product is displayed");
        WebElement productTitle = searchProductPage.getProductTitleByName(product);

        assertTrue(productTitle.isDisplayed(),
                "Item with product name = \"" + product + "\" was not displayed");

        Utils.log("Opens product page");
        productTitle.click();

        ProductPage productPage = new ProductPage(driver);

        Utils.log("Validates basic product displayed data");
        assertTrue(productPage.getProductTitle().getText().toUpperCase().contains(product.toUpperCase()),
                "Displayed Product title different from expected (" + product + ")");

        assertTrue(productPage.getProductPrice().getText().contains(price),
                "Displayed product price different from expected (" + price + ")");

        CartPage cartPage = productPage.addToCart(color, size, quantity);

        Utils.log("Checks if product was added to cart");
        assertTrue(cartPage.getSuccessMsgAlert().getText().toUpperCase().contains(product.toUpperCase()),
                "Success alert message does not contain added product = " + product);

        assertTrue(cartPage.getProductNameLnk().getText().equalsIgnoreCase(product),
                "Product title with name =  \"" + product + "\" was not displayed");

        assertTrue(cartPage.getProductPrice().getText().contains(price),
                "Product price with price =  \"" + price + "\" was not displayed");


    }

}
