package scripts;

import entities.User;
import io.qameta.allure.Description;
import org.testng.annotations.*;
import pages.*;
import utils.Utils;

import static org.testng.Assert.assertTrue;

public class AddToWishlistTest extends BaseTest {

    private String firstName = "Test";
    private String lastName = "Test";
    private String email = Utils.generateRandomEmail();
    private String password = "Test1234";
    private String confirmPassword = "Test1234";
    private String product = "Stretch Cotton Blazer";
    private String price = "$490.00";
    private User user;

    @BeforeMethod
    @Description("Add to wishlist test precondition: register an account")
    public void before() throws Exception {

        Utils.log("Opens main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        RegisterAccountPage registerAccountPage = headerPage.goToRegisterAccount();

        user = new User(firstName, lastName, email, password, confirmPassword);

        registerAccountPage.registerAccount(user);

        tearDown();

        setUp();

    }

    @Test
    @Description("Should be able to search for an existing product via search bar then add it to the wishlist")
    public void addToWishList() throws Exception {

        Utils.log("Opens main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        LoginPage loginPage = headerPage.goToLogin();

        DashboardPage dashboardPage = loginPage.login(user);

        Utils.log("Checks if user was logged in");
        assertTrue(dashboardPage.getHelloMsgStr().getText().contains(firstName + " " + lastName),
                "Hello message does not contain user's first and last name");

        SearchProductPage searchProductPage = dashboardPage.searchItem(product);

        Utils.log("Checks if searched product is displayed");
        assertTrue(searchProductPage.getProductTitleByName(product).isDisplayed(),
                "Item with product name = \"" + product + "\" was not displayed");

        WishListPage wishListPage = searchProductPage.addToWishList(product);

        Utils.log("Checks product was successfully added to the wishlist");
        assertTrue(wishListPage.getSuccessMsgAlert().getText().toUpperCase().contains(product.toUpperCase()),
                "Success alert message does not contain expected text \""+ product + "\"");

        assertTrue(wishListPage.getProductTitleByName(product).getText().equalsIgnoreCase(product),
                "Product title with name = \"" + product + "\" was not displayed");

        assertTrue(wishListPage.getPriceByProductName(product).getText().equalsIgnoreCase(price),
                "Product price with price = \"" + price + "\" was not displayed");


    }

}
