package scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;

public class BaseTest {

    protected WebDriver driver = null;
    protected WebDriverWait wait;
    private StringBuffer verificationErrors = new StringBuffer();

    @Step("Opens a new browser session")
    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {

        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();

        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    }

    protected String getMainUrl() {
        return "http://magento-demo.lexiconn.com/";
    }

    @Step("Closes browser session")
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {

        driver.quit();

        String verificationErrorString = verificationErrors.toString();

        if (!"".equals(verificationErrorString)) {

            fail(verificationErrorString);

        }

    }

}
