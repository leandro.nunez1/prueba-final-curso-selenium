package scripts;

import entities.User;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.DashboardPage;
import pages.HeaderPage;
import pages.LoginPage;
import pages.RegisterAccountPage;
import utils.Utils;

import static org.testng.Assert.assertTrue;

public class LoginTest extends BaseTest {

    private String firstName = "Test";
    private String lastName = "Test";
    private String email = Utils.generateRandomEmail();
    private String password = "Test1234";
    private String confirmPassword = "Test1234";
    private User user;

    @BeforeMethod
    @Description("Login test precondition: register an account")
    public void before() throws Exception {

        Utils.log("Opens main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        RegisterAccountPage registerAccountPage = headerPage.goToRegisterAccount();

        user = new User(firstName, lastName, email, password, confirmPassword);

        registerAccountPage.registerAccount(user);

        tearDown();

        setUp();

    }

    @Test
    @Description("Should be able to login with a valid existing account")
    public void login() throws Exception {

        Utils.log("Opens main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        LoginPage loginPage = headerPage.goToLogin();

        DashboardPage dashboardPage = loginPage.login(user);

        Utils.log("Checks if user was logged in");
        assertTrue(dashboardPage.getHelloMsgStr().getText().contains(firstName + " " + lastName),
                "Hello message does not contain user's first and last name");

        String contactInfoTxt = dashboardPage.getContactInfoTxt().getText();

        Utils.log("Validates contact info");
        assertTrue(contactInfoTxt.contains(email),
                "Contact information section should display email = \"" + email + "\"");

        assertTrue(contactInfoTxt.contains(firstName + " " + lastName),
                "Contact information section should display account firstname and lastname");

    }
}
