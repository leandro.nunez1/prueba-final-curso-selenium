package scripts;

import dataproviders.UsersData;
import entities.User;
import io.qameta.allure.Description;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pages.*;
import utils.Utils;

import static org.testng.Assert.assertTrue;

public class RegisterAccountTest extends BaseTest {

    private String pageTitle = "CREATE AN ACCOUNT";
    private final String successMsg = "Thank you for registering with Madison Island.";

    @Test(dataProvider = "users", dataProviderClass = UsersData.class)
    @Description("Should be able to register an account using valid input data")
    public void registerAccount(String firstName, String lastName, String email, String password, String confirmPassword) throws Exception {

        Utils.log("Open main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        RegisterAccountPage registerAccountPage = headerPage.goToRegisterAccount();

        assertTrue(registerAccountPage.getPageTitle().getText().equalsIgnoreCase(pageTitle),
                "Displayed page title different from expected \"" + pageTitle + "\" page title");

        User user = new User(firstName, lastName, email, password, confirmPassword);

        DashboardPage dashboardPage = registerAccountPage.registerAccount(user);

        WebElement successMsgAlert = dashboardPage.getSuccessMsgAlert();

        assertTrue(successMsgAlert.isDisplayed(), "Success alert message was not displayed");

        assertTrue(successMsgAlert.getText().equals(successMsg),
                "Expected success alert message to equal \"" + successMsg + "\"");

    }

}
