package scripts;

import io.qameta.allure.Description;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HeaderPage;
import pages.SearchProductPage;
import utils.Utils;

import static org.testng.Assert.assertTrue;

public class SearchProductTest extends BaseTest {

    private String product = "Stretch Cotton Blazer";
    private String price = "$490.00";

    @Parameters({"productParam","priceParam"})
    @Test
    @Description("Should be able to search for an existing product via the header search bar")
    public void searchProduct(String product,String price) throws Exception {

        Utils.log("Opens main URL");
        driver.get(getMainUrl());

        HeaderPage headerPage = new HeaderPage(driver);

        SearchProductPage searchProductPage = headerPage.searchItem(product);

        Utils.log("Checks if searched product is displayed");
        assertTrue(searchProductPage.getPageTitle().getText().toUpperCase().contains(product.toUpperCase()),
                "Page title does not contain searched product name");

        assertTrue(searchProductPage.getProductTitleByName(product).isDisplayed(),
                "Item with product name = \"" + product + "\" was not displayed");

        assertTrue(searchProductPage.getPriceByProductName(product).getText().contains(price),
                "Displayed product price different from expected (" + price + ")");

    }

}
