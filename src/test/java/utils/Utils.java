package utils;

import io.qameta.allure.Step;

import java.util.UUID;

public final class Utils {

    public static String getUniqueId() {
        return String.format("%s_%s", UUID.randomUUID().toString().substring(0, 5), System.currentTimeMillis() / 1000);
    }

    public static String generateRandomEmail() {
        return String.format("%s@%s", getUniqueId(), "yourHostName.com");
    }

    @Step("{0}")
    public static void log(final String message){
        //intentionally empty
    }

}
